
# tup-scaffold

An example project that demonstrates how to build a simple command line tool, based on a static library, with [tup](http://gittup.org/tup).

## build & test it

    $ tup
    $ ... tup initializes project, and build completes ...
    $ ./cmd/cmd
    $ "hello from libexample"
